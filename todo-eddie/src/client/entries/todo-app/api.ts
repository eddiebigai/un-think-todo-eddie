import {TodoModel} from '../../../server/models/todo-model';
import {AuthorizationError, request, UnthinkApiResponse} from '../../api/unthink-api';

async function handleError(response: UnthinkApiResponse): Promise<unknown> {
  let errorModel: unknown;
  try {
    errorModel = await response.error();
  } catch (err) {
    if (err instanceof AuthorizationError) {
      window.location.reload();
    }
    return err;
  }
  return errorModel;
}

export async function getTodos(): Promise<TodoModel[]> {

	const response = await window.fetch(
		'api/todo',
		{
			headers: {
				'Content-Type': 'application/json'
			}
		}
	);

	return (await response.json()).map((record: Record<string, unknown>) => new TodoModel(record));
}

export async function postTodo(todo: TodoModel): Promise<void> {

	const response = await request(
		'api/todo',
		{
			method: 'POST',
			body: JSON.stringify({
	        	title: todo.title
	      })
		}
	);

  if (!response.ok) {
    throw await handleError(response);
  }
}

export async function completeTodo(todo: TodoModel): Promise<void> {

	console.log(todo);

	const response = await request(
		'api/todo',
		{
			method: 'PUT',
			body: JSON.stringify(todo)
		}
	);

	if (!response.ok) {
		throw await handleError(response);
  }
}

export async function deleteTodo(todoID: number): Promise<void> {

	const response = await request(
		'api/todo/'+todoID,
		{
			method: 'DELETE',
			body: JSON.stringify({ todoID: todoID})
		}
	);

	if (!response.ok) {
		throw await handleError(response);
  }
}