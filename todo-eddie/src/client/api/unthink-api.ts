export class AuthorizationError extends Error {
  constructor() {
    super('Unauthorized');
  }
}

export class ResourceNotFoundError extends Error {
  constructor() {
    super('Resource not found');
  }
}

export class UnthinkApiResponse {

  private readonly response: Response;
  
  constructor(resp: Response) {
    this.response = resp;
  }

  get ok(): boolean {
    return this.response.ok;
  }

  get status(): number {
    return this.response.status;
  }

  async value<T>(): Promise<T> {
    if (!this.ok) {
      throw new Error('API response was an error response. Please call the error function instead.');
    }

    return (await this.response.json()) as T;
  }
  
  async error<T>(): Promise<T> {
    // unauthorized error
    if (this.response.status === 401) {
      throw new AuthorizationError();
    }

    // If status is 400 this is an API error
    if (this.response.status === 400) {
      const model = await this.response.json();
      return model as T;
    }

    if (this.response.status === 404) {
      throw new ResourceNotFoundError();
    }

    let unknownError: object;
    try {
      unknownError = await this.response.json();
    } catch (e) {
      unknownError = e;
    }

    throw unknownError;
  }
}

export async function request(
  info: RequestInfo,
  init: RequestInit = { headers: { 'Content-Type': 'application/json'}}): Promise<UnthinkApiResponse> {
  // Default to always having 'application/json' for Content-Type
  if (!init.headers) {
    init.headers = {
      'Content-Type': 'application/json'
    };
  } else if (!init.headers.hasOwnProperty('Content-Type')) {
    init.headers = {
      ...init.headers,
      'Content-Type': 'application/json'
    };
  }
  const requestResponse = await window.fetch(
    info,
    init
  );
  return new UnthinkApiResponse(requestResponse);
}