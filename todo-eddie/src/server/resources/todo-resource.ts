import { ResourceBase } from './resource-base';
import { resource, get, post, put, del, body, path, template, TemplateResponse, RedirectResponse, ApiResponse, CookieResponse } from 'resource-decorator';
import { TodoModel } from '../models/todo-model';

const _allTodos: TodoModel[] = [

  new TodoModel({

    id: 0,
    title: "First Item.",
    completed: true,
    dateCreated: new Date('05/1/1999')
  }),
  new TodoModel({

    id: 1,
    title: "Second Item.",
    completed: false,
    dateCreated: new Date('1/1/2020')
  }),
];

@resource({
  basePath: '',
})
export class TodoResource extends ResourceBase {
  @template()
  async todoPage(): Promise<TemplateResponse | RedirectResponse> {
    return new TemplateResponse('todo.html');
  }

  @get({
    path: '/api/todo'
  })
  async getMessage(): Promise<ApiResponse | CookieResponse | void> {
    return new ApiResponse(_allTodos);
  }

  @post({
    path: '/api/todo'
  })
  async postTodo(@body() model: TodoModel): Promise<ApiResponse | CookieResponse | void> {
    
    const newTodo: TodoModel = model;
    
    newTodo.id = _allTodos.length;
    newTodo.dateCreated = new Date();

    const newID: number = newTodo.id;
    _allTodos.push(newTodo);

    return new ApiResponse(newID);
  }

  @put({
    path: '/api/todo'
  })
  async completeTodo(@body() model: TodoModel): Promise<ApiResponse | CookieResponse | void> {

    const mID: number = model.id!;

    for (var i = 0; i < _allTodos.length; ++i) {
      
      if (_allTodos[i].id === mID) {

        const todo = _allTodos[i];
        todo.completed = true;

        _allTodos[i] = todo;

        break;
      }
    }

    return new ApiResponse(mID);
  }

  @del({
    path: '/api/todo/:id'
  })
  async deleteTodo(@path('id') id: string): Promise<ApiResponse | CookieResponse | void> {

    _allTodos.splice(Number(id), 1);
  }
}
