"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const resource_base_1 = require("./resource-base");
const resource_decorator_1 = require("resource-decorator");
const todo_model_1 = require("../models/todo-model");
const _allTodos = [
    new todo_model_1.TodoModel({
        id: 0,
        title: "First Item.",
        completed: true,
        dateCreated: new Date('05/1/1999')
    }),
    new todo_model_1.TodoModel({
        id: 1,
        title: "Second Item.",
        completed: false,
        dateCreated: new Date('1/1/2020')
    }),
];
let TodoResource = class TodoResource extends resource_base_1.ResourceBase {
    async todoPage() {
        return new resource_decorator_1.TemplateResponse('todo.html');
    }
    async getMessage() {
        return new resource_decorator_1.ApiResponse(_allTodos);
    }
    async postTodo(model) {
        const newTodo = model;
        newTodo.id = _allTodos.length;
        newTodo.dateCreated = new Date();
        const newID = newTodo.id;
        _allTodos.push(newTodo);
        return new resource_decorator_1.ApiResponse(newID);
    }
    async completeTodo(model) {
        const mID = model.id;
        for (var i = 0; i < _allTodos.length; ++i) {
            if (_allTodos[i].id === mID) {
                const todo = _allTodos[i];
                todo.completed = true;
                _allTodos[i] = todo;
                break;
            }
        }
        return new resource_decorator_1.ApiResponse(mID);
    }
    async deleteTodo(id) {
        _allTodos.splice(Number(id), 1);
    }
};
__decorate([
    resource_decorator_1.template(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TodoResource.prototype, "todoPage", null);
__decorate([
    resource_decorator_1.get({
        path: '/api/todo'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TodoResource.prototype, "getMessage", null);
__decorate([
    resource_decorator_1.post({
        path: '/api/todo'
    }),
    __param(0, resource_decorator_1.body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [todo_model_1.TodoModel]),
    __metadata("design:returntype", Promise)
], TodoResource.prototype, "postTodo", null);
__decorate([
    resource_decorator_1.put({
        path: '/api/todo'
    }),
    __param(0, resource_decorator_1.body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [todo_model_1.TodoModel]),
    __metadata("design:returntype", Promise)
], TodoResource.prototype, "completeTodo", null);
__decorate([
    resource_decorator_1.del({
        path: '/api/todo/:id'
    }),
    __param(0, resource_decorator_1.path('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TodoResource.prototype, "deleteTodo", null);
TodoResource = __decorate([
    resource_decorator_1.resource({
        basePath: '',
    })
], TodoResource);
exports.TodoResource = TodoResource;
//# sourceMappingURL=todo-resource.js.map